
docker container stop couchdb
docker container rm -f couchdb
docker container stop couchdb1
docker container rm -f couchdb1

docker run  -e COUCHDB_USER=admin -e COUCHDB_PASSWORD=password --name=couchdb -p 5984:5984 -d couchdb:3.1.1 --restart unless-stopped
docker run  -e COUCHDB_USER=admin -e COUCHDB_PASSWORD=password --name=couchdb1 -p 6984:5984 -d couchdb:3.1.1 --restart unless-stopped

sleep 3s

# sudo curl localhost:5984

# sudo curl localhost:6984

