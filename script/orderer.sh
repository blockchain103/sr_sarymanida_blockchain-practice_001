
export PATH=${PWD}/bin:$PATH
export ORDERER_FILELEDGER_LOCATION=../ledger-org1/
export FABRIC_LOGGING_SPEC=INFO
export FABRIC_CFG_PATH=${PWD}/config
export ORDERER_GENERAL_LISTENADDRESS=0.0.0.0
export ORDERER_GENERAL_GENESISMETHOD=file
export ORDERER_GENERAL_GENESISFILE=../artifacts/genesis.block
export ORDERER_GENERAL_LOCALMSPID=OrdererMSP
export ORDERER_GENERAL_LOCALMSPDIR=../crypto-config/ordererOrganizations/example.com/users/Admin@example.com/msp
export CORE_PEER_TLS_ROOTCERT_FILE=../crypto-config/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem
export ORDERER_GENERAL_LOCALMSPDIR=../crypto-config/ordererOrganizations/example.com/orderers/orderer.example.com/msp
export ORDERER_GENERAL_TLS_PRIVATEKEY=../crypto-config/ordererOrganizations/example.com/orderers/orderer.example.com/tls/server.key
export ORDERER_GENERAL_TLS_CERTIFICATE=../crypto-config/ordererOrganizations/example.com/orderers/orderer.example.com/tls/server.crt
export ORDERER_GENERAL_TLS_ROOTCAS=../crypto-config/ordererOrganizations/example.com/orderers/orderer.example.com/tls/ca.crt

orderer